package com.example.demo;

public class BookIdMismatchException extends Exception {
    public BookIdMismatchException(String errorMessage) {
        super(errorMessage);
    }
}
